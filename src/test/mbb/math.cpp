//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>

#include <gtest/gtest.h>

#include "mbb/math.h"

TEST(MathTest, Min) {
  EXPECT_EQ(bMin(1, 1), 1);
  EXPECT_EQ(bMin(1, 2), 1);
  EXPECT_EQ(bMin(2, 1), 1);

  EXPECT_EQ(bMin(0, 0), 0);

  EXPECT_EQ(bMin(1, 0), 0);
  EXPECT_EQ(bMin(0, 1), 0);

  EXPECT_EQ(bMin(-1, 0), -1);
  EXPECT_EQ(bMin(0, -1), -1);

  EXPECT_EQ(bMin(-1, -2), -2);
  EXPECT_EQ(bMin(-2, -1), -2);
}

TEST(MathTest, Floor) {
  EXPECT_EQ(bFloor(1.25f), 1);
  EXPECT_EQ(bFloor(1.50f), 1);
  EXPECT_EQ(bFloor(1.75f), 1);

  EXPECT_EQ(bFloor(0.25f), 0);
  EXPECT_EQ(bFloor(0.50f), 0);
  EXPECT_EQ(bFloor(0.75f), 0);

  EXPECT_EQ(bFloor(-0.25f), -1);
  EXPECT_EQ(bFloor(-0.50f), -1);
  EXPECT_EQ(bFloor(-0.75f), -1);
}

TEST(MathTest, Clamp2Uint8) {
  EXPECT_EQ(bClamp2Uint8(256.0f), 255);
  EXPECT_EQ(bClamp2Uint8(-1.0f), 0);
  EXPECT_EQ(bClamp2Uint8(128.0f), 128);
}

TEST(MathTest, Clamp) {
  EXPECT_EQ(bClamp(5, 0, 639), 5);
  EXPECT_EQ(bClamp(-1, 0, 639), 0);
  EXPECT_EQ(bClamp(640, 0, 639), 639);
}

TEST(MathTest, MirroredClamp) {
  EXPECT_EQ(bMirroredClamp(5, 0, 639), 5);
  EXPECT_EQ(bMirroredClamp(-1, 0, 639), 1);
  EXPECT_EQ(bMirroredClamp(-2, 0, 639), 2);
  EXPECT_EQ(bMirroredClamp(640, 0, 639), 638);
  EXPECT_EQ(bMirroredClamp(641, 0, 639), 637);
}
