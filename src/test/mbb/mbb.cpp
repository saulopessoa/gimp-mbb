//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>

#include <boost/gil/extension/io/png_dynamic_io.hpp>
#include <boost/gil/image.hpp>
#include <gtest/gtest.h>

#include "mbb/mbb.h"

std::vector<buint8> GilView2Raw(const boost::gil::rgb8c_view_t& view) {
  const int bands = view.num_channels();

  std::vector<buint8> result;
  result.reserve(view.width()*view.height()*bands);
  for (const auto& p : view)
    for (int b = 0; b < bands; ++b)
      result.push_back(p[b]);

  return result;
}

TEST(MbbTest, bBlend) {
  boost::gil::rgb8_image_t img0, img1, mask, gt;
  boost::gil::png_read_image("../gimp-mbb/resource/test/apple.png", img0);
  boost::gil::png_read_image("../gimp-mbb/resource/test/pear.png", img1);
  boost::gil::png_read_image("../gimp-mbb/resource/test/mask.png", mask);
  boost::gil::png_read_image("../gimp-mbb/resource/test/gt.png", gt);
  boost::gil::rgb8c_view_t img0_v, img1_v, mask_v, gt_v;
  img0_v = boost::gil::const_view(img0);
  img1_v = boost::gil::const_view(img1);
  mask_v = boost::gil::const_view(mask);
  gt_v = boost::gil::const_view(gt);

  ASSERT_EQ(img0_v.dimensions(), img1_v.dimensions());
  ASSERT_EQ(img0_v.num_channels(), img1_v.num_channels());
  ASSERT_EQ(img0_v.dimensions(), mask_v.dimensions());
  ASSERT_EQ(img0_v.num_channels(), mask_v.num_channels());
  ASSERT_EQ(img0_v.dimensions(), gt_v.dimensions());
  ASSERT_EQ(img0_v.num_channels(), gt_v.num_channels());

  const int width = img0_v.width();
  const int height = img0_v.height();
  const int bands = img0_v.num_channels();

  std::vector<buint8> img0_vec = GilView2Raw(img0_v);
  std::vector<buint8> img1_vec = GilView2Raw(img1_v);
  std::vector<buint8> mask_vec = GilView2Raw(mask_v);
  std::vector<buint8> gt_vec = GilView2Raw(gt_v);
  std::vector<buint8> out_vec(img0_vec.size());

  ASSERT_NE(bBlend(width, height, bands, bkUint8, img0_vec.data(),
                   img1_vec.data(), mask_vec.data(), out_vec.data(), nullptr),
            nullptr);

//  ASSERT_TRUE(std::equal(out_vec.begin(), out_vec.end(), gt_vec.begin()));
}
