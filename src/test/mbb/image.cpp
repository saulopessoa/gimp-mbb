//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>

#include <gtest/gtest.h>

#include "mbb/image.h"

class bImageTest : public testing::Test {
 protected:
  void SetUp() override {
    width_ = 320;
    height_ = 240;
    bands_ = 3;

    const int size = width_*height_*bands_;

    bInitImg(&img0_, width_, height_, bands_);
    bInitImg(&img1_, width_, height_, bands_);

    for (int i = 0; i < size; ++i)
      img0_.data[i] = img1_.data[i] = i;
  }

  void TearDown() override {
    bReleaseImg(&img0_);
    bReleaseImg(&img1_);
  }

  int width_;
  int height_;
  int bands_;

  bImage img0_;
  bImage img1_;
  bImage img2_;
};

TEST(ImageTest, InitRelease) {
  EXPECT_EQ(bInitImg(nullptr, 640, 480, 3), nullptr);

  bImage img;
  ASSERT_NE(bInitImg(&img, 640, 480, 3), nullptr);
  EXPECT_EQ(img.width, 640);
  EXPECT_EQ(img.height, 480);
  EXPECT_EQ(img.bands, 3);
  EXPECT_NE(img.data, nullptr);

  EXPECT_EQ(bReleaseImg(nullptr), nullptr);
  EXPECT_NE(bReleaseImg(&img), nullptr);
}

TEST(ImageTest, ColorOps) {
  const int bands = 3;

  const breal color_a[] = {1.f, 2.f, 3.f};
  const breal color_b[] = {2.f, 4.f, 6.f};
  breal color_o[] = {0.f, 0.f, 0.f};

  bCopyColor(bands, color_a, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, color_a));

  const breal sub_result0[] = {1.f, 2.f, 3.f};
  bSubColor(bands, color_b, color_a, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, sub_result0));
  const breal sub_result1[] = {-1.f, -2.f, -3.f};
  bSubColor(bands, color_a, color_b, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, sub_result1));

  const breal add_result[] = {3.f, 6.f, 9.f};
  bAddColor(bands, color_a, color_b, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, add_result));
  bAddColor(bands, color_b, color_a, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, add_result));

  const breal mul_result[] = {2.f, 8.f, 18.f};
  bMulColor(bands, color_a, color_b, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, mul_result));
  bMulColor(bands, color_b, color_a, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, mul_result));

  bMulsColor(bands, color_a, 2.f, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, color_b));
  bMulsColor(bands, color_b, 0.5f, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, color_a));

  const breal fill_result[] = {-1.f, -1.f, -1.f};
  bFillColor(bands, -1.f, color_o);
  EXPECT_TRUE(std::equal(color_o, color_o+bands, fill_result));
}

TEST_F(bImageTest, At) {
  int w = img0_.width/2, h = img0_.height/2;
  int coord_idx = (img1_.width*h + w)*img1_.bands;
  for (int i = 0; i < img0_.bands; ++i)
    img0_.data[coord_idx + i] = 1.1f+i;

  breal* color = bAt(&img0_, w, h);
  for (int i = 0; i < img0_.bands; ++i)
    EXPECT_FLOAT_EQ(color[i], 1.1f+i) << "Failed at index " << i;
}

TEST_F(bImageTest, CopyImg) {
  const int size = width_*height_*bands_;

  bCopyImg(&img0_, &img2_, true);
  ASSERT_NE(img0_.data, img2_.data);
  ASSERT_TRUE(std::equal(img0_.data, img0_.data+size, img2_.data));

  std::fill(img0_.data, img0_.data+size, 1.f);
  bCopyImg(&img0_, &img2_, false);
  ASSERT_TRUE(std::equal(img0_.data, img0_.data+size, img2_.data));

  bReleaseImg(&img2_);
}

TEST_F(bImageTest, SubImg) {
  const int size = width_*height_*bands_;

  bSubImg(&img0_, &img1_, &img2_, true);
  ASSERT_NE(img0_.data, img2_.data);
  ASSERT_NE(img1_.data, img2_.data);
  for (int i = 0; i < size; ++i)
    ASSERT_FLOAT_EQ(img2_.data[i], img0_.data[i]-img1_.data[i]);

  std::fill(img2_.data, img2_.data+size, 1.f);
  bSubImg(&img0_, &img1_, &img2_, false);
  for (int i = 0; i < size; ++i)
    ASSERT_FLOAT_EQ(img2_.data[i], img0_.data[i]-img1_.data[i]);

  bReleaseImg(&img2_);
}

TEST_F(bImageTest, AddImg) {
  const int size = width_*height_*bands_;

  bAddImg(&img0_, &img1_, &img2_, true);
  ASSERT_NE(img0_.data, img2_.data);
  ASSERT_NE(img1_.data, img2_.data);
  for (int i = 0; i < size; ++i)
    ASSERT_FLOAT_EQ(img2_.data[i], img0_.data[i]+img1_.data[i]);

  std::fill(img2_.data, img2_.data+size, 1.f);
  bAddImg(&img0_, &img1_, &img2_, false);
  for (int i = 0; i < size; ++i)
    ASSERT_FLOAT_EQ(img2_.data[i], img0_.data[i]+img1_.data[i]);

  bReleaseImg(&img2_);
}
