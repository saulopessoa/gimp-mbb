//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include <gtest/gtest.h>

#include "mbb/image_pyramid.h"

TEST(ImagePyramidTest, InitRelease) {
  const int pyr_height = 3;
  const int img_width = 320;
  const int img_height = 240;
  const int img_bands = 2;

  ASSERT_EQ(bInitImgPyramid(nullptr, pyr_height), nullptr);

  bImagePyramid img_pyramid;
  ASSERT_NE(bInitImgPyramid(&img_pyramid, pyr_height), nullptr);
  ASSERT_EQ(img_pyramid.height, pyr_height);
  ASSERT_NE(img_pyramid.levels, nullptr);

  bImage imgs[pyr_height];
  for (int i = 0; i < pyr_height; ++i) {
    ASSERT_NE(bInitImg(&imgs[i], img_width, img_height, img_bands), nullptr);
    img_pyramid.levels[i] = imgs[i];
  }

  ASSERT_EQ(bReleaseImgPyramid(nullptr), nullptr);
  ASSERT_NE(bReleaseImgPyramid(&img_pyramid), nullptr);
}
