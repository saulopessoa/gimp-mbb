//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include "mbb/image_pyramid.h"

#include <stdlib.h>

bImagePyramid* bInitImgPyramid(bImagePyramid* img_pyramid, int height) {
  if (!img_pyramid) return NULL;

  bImage* imgs = (bImage*)malloc(height*sizeof(bImage));
  if (!imgs) return NULL;

  for (int i = 0; i < height; ++i)
    imgs[i].data = NULL;

  img_pyramid->height = height;
  img_pyramid->levels = imgs;
  return img_pyramid;
}

bImagePyramid* bReleaseImgPyramid(bImagePyramid* img_pyramid) {
  if (!img_pyramid) return NULL;

  for (int i = 0; i < img_pyramid->height; ++i)
    bReleaseImg(&img_pyramid->levels[i]);

  free(img_pyramid->levels);
  return img_pyramid;
}
