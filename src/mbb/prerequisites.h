//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#ifndef MBB_PREREQUISITES_H_
#define MBB_PREREQUISITES_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Available input/output data types enums. */
typedef enum {
  bkUint8 = 0
} bDataType;

// Integer types used internally.
typedef uint8_t     buint8;

// Floating point types.
typedef float       breal;

// Maximum number of bands a color can have.
#define kMaxBands   4

#ifdef __cplusplus
}
#endif

#endif  // MBB_PREREQUISITES_H_
