//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include "mbb/mbb.h"

#include "mbb/image.h"
#include "mbb/image_pyramid.h"
#include "mbb/math.h"

static const breal kBlack[kMaxBands] = {0.f, 0.f, 0.f, 0.f};

/* 1D weight function (it is equivalent to a gaussian function). */
static breal bWeight1D(int v) {
  const breal a = 0.4f;
  const breal b = 1.0f/4.0f;
  const breal c = 1.0f/4.0f - a/2.0f;

  if (v == 0)
    return a;
  else if (v == -1 || v == 1)
    return b;
  else if (v == -2 || v == 2)
    return c;

  return 0;
}

/* 2D weight function. */
static breal bWeight2D(int m, int n) {
  return bWeight1D(m)*bWeight1D(n);
}

/* Samples color from image. It is aware of image bounds and return proper
values when outer pixels are requested. */
static breal* bSampleB(const bImage* img, int x, int y, breal* out) {
  if (y < 0 || x < 0 ||
      y >= img->height || x >= img->width) {
    const int clamped_w = bClamp(x, 0, img->width-1);
    const int clamped_h = bClamp(y, 0, img->height-1);
    const int mir_clamp_w = bMirroredClamp(x, 0, img->width-1);
    const int mir_clamp_h = bMirroredClamp(y, 0, img->height-1);

    breal buf[kMaxBands];
    bMulsColor(img->bands, bAt(img, clamped_w, clamped_h), 2, buf);
    return bSubColor(img->bands, buf, bAt(img, mir_clamp_w, mir_clamp_h), out);
  }

  return bCopyColor(img->bands, bAt(img, x, y), out);
}

/* Reduces image. */
static bImage* bReduce(const bImage* img, bImage* reduced_img) {
  bInitImg(reduced_img, (img->width+1)/2, (img->height+1)/2, img->bands);
  bFillImg(reduced_img, kBlack);

  for (int y = 0; y < reduced_img->height; ++y) {
    for (int x = 0; x < reduced_img->width; ++x) {
      breal* sample = bAt(reduced_img, x, y);

      for (int mi = -2; mi <= 2; ++mi) {
        for (int ni = -2; ni <= 2; ++ni) {
          breal buf0[kMaxBands];
          breal buf1[kMaxBands];

          bMulsColor(img->bands, bSampleB(img, x*2+ni, y*2+mi, buf0),
                     bWeight2D(mi, ni), buf1);

          bAddColor(reduced_img->bands, sample, buf1, sample);
        }
      }
    }
  }

  return reduced_img;
}

/* Expands image. */
static bImage* bExpand(const bImage* img, bImage* expanded_img) {
  bInitImg(expanded_img, img->width*2 - 1, img->height*2 - 1, img->bands);
  bFillImg(expanded_img, kBlack);

  for (int y = 0; y < expanded_img->height; ++y) {
    for (int x = 0; x < expanded_img->width; ++x) {
      breal* sample = bAt(expanded_img, x, y);

      for (int mi = -2; mi <= 2; ++mi) {
        for (int ni = -2; ni <= 2; ++ni) {
          if ((y+mi)%2 == 0 && (x+ni)%2 == 0) {
            breal buf0[kMaxBands];
            breal buf1[kMaxBands];

            bMulsColor(img->bands, bSampleB(img, (x+ni)/2, (y+mi)/2, buf0),
                       bWeight2D(mi, ni), buf1);

            bAddColor(expanded_img->bands, sample, buf1, sample);
          }
        }
      }
      bMulsColor(expanded_img->bands, sample, 4, sample);
    }
  }

  return expanded_img;
}

/* Evaluates the height of a pyramid given its largest image. */
static int bEvalPyramidHeight(const bImage* img) {
  int smaller_dimension = bMin(img->width, img->height) - 1;
  int tmp_shift = smaller_dimension;
  int counter = 0;

  while (tmp_shift >= 1) {
    tmp_shift = tmp_shift >> 1;
    ++counter;
  }

  return counter;
}

/* Builds a gaussian pyramid from the first image (G0). */
static bImagePyramid *bBuildG(const bImage* G0, bImagePyramid* G) {
  int pyr_height = bEvalPyramidHeight(G0);

  bInitImgPyramid(G, pyr_height);
  G->levels[0] = *G0;

  for (int i = 0; i < G->height-1; ++i) {
    bImage Gip1;
    bReduce(&G->levels[i], &Gip1);
    G->levels[i+1] = Gip1;
  }

  return G;
}

/* Builds a laplacian pyramid from a gaussian pyramid. */
static bImagePyramid *bBuildL(const bImagePyramid* G, bImagePyramid* L) {
  bInitImgPyramid(L, G->height);
  // A deep copy is necessary because this image will be after deleted twice
  // (once from the gaussian pyramid and once from the laplacian pyramid)
  bCopyImg(&G->levels[G->height-1], &L->levels[L->height-1], true);

  for (int i = 0; i < L->height-1; ++i) {
    bImage Li;
    bExpand(&G->levels[i+1], &Li);
    bSubImg(&G->levels[i], &Li, &Li, false);
    L->levels[i] = Li;
  }

  return L;
}

/* Blends two images according to a mask. */
static bImage *bBlendImgs(const bImage* img0, const bImage* img1,
                          const bImage* mask, bImage* out,
                          ProgressCallback progress) {
  bImagePyramid G_img0, L_img0,  // Img0 gaussian and laplacian pyramid
                G_img1, L_img1,  // Img1 gaussian and laplacian pyramid
                G_mask,          // Mask laplacian pyramid
                L_blended;       // Blended laplacian pyramid
  bBuildG(img0, &G_img0);
  bBuildL(&G_img0, &L_img0);
  // TODO(sap): Maybe an earlier release of pyramids G_img0 and G_img1 could
  // reduce memory footprint.

  if (progress) progress(0.35);

  bBuildG(img1, &G_img1);
  bBuildL(&G_img1, &L_img1);

  if (progress) progress(0.55);

  bBuildG(mask, &G_mask);

  if (progress) progress(0.65);

  bInitImgPyramid(&L_blended, L_img0.height);

  for (int i = 0; i < L_blended.height; ++i) {
    const bImage* Li_img0 = &L_img0.levels[i];
    const bImage* Li_img1 = &L_img1.levels[i];
    const bImage* Gi_mask = &G_mask.levels[i];
    bImage Li_blended;

    bInitImg(&Li_blended, Li_img0->width, Li_img0->height, Li_img0->bands);

    for (int y = 0; y < Li_blended.height; ++y) {
      for (int x = 0; x < Li_blended.width; ++x) {
        breal buf0[kMaxBands];
        breal buf1[kMaxBands];

        breal* Li_blended_samp = bAt(&Li_blended, x, y);
        const breal* Li_img0_samp = bAt(Li_img0, x, y);
        const breal* Li_img1_samp = bAt(Li_img1, x, y);
        const breal* Gi_mask_samp = bAt(Gi_mask, x, y);

        bMulColor(Li_blended.bands, Li_img0_samp, Gi_mask_samp, buf0);

        bFillColor(Li_blended.bands, 1, buf1);
        bSubColor(Li_blended.bands, buf1, Gi_mask_samp, buf1);
        bMulColor(Li_blended.bands, Li_img1_samp, buf1, buf1);

        bAddColor(Li_blended.bands, buf0, buf1, Li_blended_samp);
      }
    }
    L_blended.levels[i] = Li_blended;
  }

  if (progress) progress(0.85);

  // Restores blended image from its laplacian pyramid.
  bImage blended_img;
  bCopyImg(&L_blended.levels[L_blended.height-1], &blended_img, true);
  for (int i = L_blended.height - 2; i >= 0; --i) {
    bImage expanded_img;
    bExpand(&blended_img, &expanded_img);

    bReleaseImg(&blended_img);

    bAddImg(&L_blended.levels[i], &expanded_img, &blended_img, true);

    bReleaseImg(&expanded_img);
  }

  bReleaseImgPyramid(&G_img0);
  bReleaseImgPyramid(&L_img0);
  bReleaseImgPyramid(&G_img1);
  bReleaseImgPyramid(&L_img1);
  bReleaseImgPyramid(&G_mask);
  bReleaseImgPyramid(&L_blended);

  if (progress) progress(0.95);

  *out = blended_img;
  return out;
}

/* Evaluates a suitable size for a given dimenstion of the input image.
This operation is necessary because multi-band operations can only be
performed over images sized such as: n^2+1, for n >= 0. In order to avoid
information loss, the new size is always bigger than or equal to the original
size. */
static int bScaledDimension(int dim) {
  int tmp_shift = dim-2;
  int counter = 0;
  int result = 0;

  while (tmp_shift > 1) {
    tmp_shift = tmp_shift >> 1;
    ++counter;
  }
  result = (0x1 << (counter+1)) + 1;

  return result;
}

/* Converts data block to an image of suitable size. */
static bImage* bData2Image(int width, int height, int bands,
                           const void* data, bImage* img, breal scale) {
  int scaled_width = bScaledDimension(width);
  int scaled_height = bScaledDimension(height);

  bInitImg(img, scaled_width, scaled_height, bands);

  for (int y = 0; y < img->height; ++y) {
    float yn = y/(float)img->height + 1.0f/(2*img->height);
    int ys = bFloor(height*yn);
    for (int x = 0; x < img->width; ++x) {
      float xn = x/(float)img->width + 1.0f/(2*img->width);
      int xs = bFloor(width*xn);

      breal* img_px = bAt(img, x, y);
      int cood_idx = bCoord2Index(xs, ys, width, height, bands);
      for (int ci = 0; ci < bands; ++ci)
        img_px[ci] = scale * ((buint8*)data)[cood_idx+ci];
    }
  }

  return img;
}

/* Converts image to a data block of a given size (it is usually the input
data block original size). */
static void *bImage2Data(const bImage* img, int width, int height,
                         int bands, void* out) {
  for (int y = 0; y < height; ++y) {
    float yn = y/(float)height + 1.0f/(2*height);
    int ys = bFloor(img->height*yn);
    for (int x = 0; x < width; ++x) {
      float xn = x/(float)width + 1.0f/(2*width);
      int xs = bFloor(img->width*xn);

      breal* sample = bAt(img, xs, ys);
      int cood_idx = bCoord2Index(x, y, width, height, bands);
      for (int ci = 0; ci < bands; ++ci)
        ((buint8*)out)[cood_idx+ci] = bClamp2Uint8(sample[ci]);
    }
  }

  return out;
}

void *bBlend(int width, int height, int bands, bDataType type,
             const void* img0_data, const void* img1_data,
             const void* mask_data, void* out, ProgressCallback progress) {
  bImage img0, img1, mask;
  bImage blended;
  if (progress) progress(0.0);

  bData2Image(width, height, bands, img0_data, &img0, 1);
  if (progress) progress(0.05);
  bData2Image(width, height, bands, img1_data, &img1, 1);
  if (progress) progress(0.1);
  bData2Image(width, height, bands, mask_data, &mask, 1.0f/255);
  if (progress) progress(0.15);

  bBlendImgs(&img0, &img1, &mask, &blended, progress);

  bImage2Data(&blended, width, height, bands, out);

  bReleaseImg(&blended);
  if (progress) progress(1.0);
  return out;
}
