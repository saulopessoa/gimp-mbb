//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#include "mbb/image.h"

#include <stdlib.h>
#include <string.h>

bImage* bInitImg(bImage* img, int width, int height, int bands) {
  if (!img) return NULL;

  const int data_size = width*height*bands*sizeof(breal);
  breal* data = (breal*)malloc(data_size);

  if (!data) return NULL;

  img->width = width;
  img->height = height;
  img->bands = bands;
  img->data = data;
  return img;
}

bImage* bReleaseImg(bImage* img) {
  if (!img) return NULL;

  free(img->data);
  return img;
}

int bCoord2Index(int x, int y, int width, int height, int bands) {
  return (y*width + x)*bands;
}

breal* bAt(const bImage* img, int x, int y) {
  return &img->data[bCoord2Index(x, y, img->width, img->height, img->bands)];
}

breal* bCopyColor(int bands, const breal* c, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = c[i];
  return out;
}

breal* bSubColor(int bands, const breal* c0, const breal* c1, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = c0[i] - c1[i];
  return out;
}

breal* bAddColor(int bands, const breal* c0, const breal* c1, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = c0[i] + c1[i];
  return out;
}

breal* bMulColor(int bands, const breal* c0, const breal* c1, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = c0[i] * c1[i];
  return out;
}

breal* bMulsColor(int bands, const breal* c, breal s, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = c[i] * s;
  return out;
}

breal* bFillColor(int bands, breal s, breal* out) {
  for (int i = 0; i < bands; ++i)
    out[i] = s;
  return out;
}

bImage* bCopyImg(const bImage* a, bImage* out, bool init_out) {
  if (init_out)
    bInitImg(out, a->width, a->height, a->bands);

  memcpy(out->data, a->data, a->width*a->height*a->bands*sizeof(breal));

  return out;
}

bImage* bSubImg(const bImage* a, const bImage* b, bImage* out, bool init_out) {
  if (init_out)
    bInitImg(out, a->width, a->height, a->bands);

  for (int hi = 0; hi < a->height; ++hi)
    for (int wi = 0; wi < a->width; ++wi)
      bSubColor(a->bands, bAt(a,wi,hi), bAt(b,wi,hi), bAt(out,wi,hi));

  return out;
}

bImage* bAddImg(const bImage* a, const bImage* b, bImage* out, bool init_out) {
  if (init_out)
    bInitImg(out, a->width, a->height, a->bands);

  for (int hi = 0; hi < a->height; ++hi)
    for (int wi = 0; wi < a->width; ++wi)
      bAddColor(a->bands, bAt(a,wi,hi), bAt(b,wi,hi), bAt(out,wi,hi));

  return out;
}

bImage* bFillImg(bImage* img, const breal* c) {
  for (int hi = 0; hi < img->height; ++hi)
    for (int wi = 0; wi < img->width; ++wi)
      bCopyColor(img->bands, c, bAt(img,wi,hi));

  return img;
}
