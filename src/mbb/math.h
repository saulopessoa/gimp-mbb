//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#ifndef MBB_MATH_H_
#define MBB_MATH_H_

#include "mbb/prerequisites.h"

#ifdef __cplusplus
extern "C" {
#endif

extern inline int bMin(int a, int b) {
  return (a < b ? a : b);
}

extern inline int bFloor(breal a) {
  return (a>0 ? (int)a : (int)a-1);
}

extern inline buint8 bClamp2Uint8(breal v) {
  if (v > 255) return 255;
  else if (v < 0) return 0;
  else return (buint8)v;
}

extern inline int bClamp(int v, int min, int max) {
  if (v > max) return max;
  else if (v < min) return min;
  else return v;
}

extern inline int bMirroredClamp(int v, int min, int max) {
  if (v > max) return max-(v-max);
  else if (v < min) return min+(min-v);
  else return v;
}

#ifdef __cplusplus
}
#endif

#endif  // MBB_MATH_H_
