//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#ifndef MBB_IMAGE_PYRAMID_H_
#define MBB_IMAGE_PYRAMID_H_

#include "mbb/image.h"
#include "mbb/prerequisites.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Struct used to store gaussian and laplacian pyramids. */
typedef struct {
  int height;
  bImage* levels;
} bImagePyramid;

/* Initializes `img_pyramid` according to `height`. Since no image dimensions
are provided, this function does not initialize/create any image, but it
only allocates the array `bImagePyramid::levels` for later storing the
images. */
bImagePyramid* bInitImgPyramid(bImagePyramid* img_pyramid, int height);

/* Releases `img_pyramid`. This operation implicitely releases
each of the images stored in `bImagePyramid::levels`. */
bImagePyramid* bReleaseImgPyramid(bImagePyramid* img_pyramid);

#ifdef __cplusplus
}
#endif

#endif  // MBB_IMAGE_PYRAMID_H_
