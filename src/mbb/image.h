//  Copyright (C) 2013 Saulo A. Pessoa <saulopessoa@gmail.com>.
//
//  This file is part of MBB GIMP.
//
//  MBB GIMP is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  MBB GIMP is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with MBB GIMP. If not, see <http://www.gnu.org/licenses/>.

#ifndef MBB_IMAGE_H_
#define MBB_IMAGE_H_

#include <stdbool.h>

#include "mbb/prerequisites.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Struct used to store images. */
typedef struct {
  int width;
  int height;
  int bands;
  breal *data;
} bImage;

/* Initializes image pointed by `img`. */
bImage* bInitImg(bImage* img, int width, int height, int bands);
bImage* bReleaseImg(bImage* img);

int bCoord2Index(int x, int y, int width, int height, int bands);
/* Samples pixel at coordinate (`x`, `y`). */
breal* bAt(const bImage* img, int x, int y);

/* Copies color to `out`. */
breal* bCopyColor(int bands, const breal* c, breal* out);
/* Subtracts colors (channel-by-channel) and places result in `out`. */
breal* bSubColor(int bands, const breal* c0, const breal* c1, breal* out);
breal* bAddColor(int bands, const breal* c0, const breal* c1, breal* out);
breal* bMulColor(int bands, const breal* c0, const breal* c1, breal* out);
/* Multiplies color by an scalar and places result in `out`. */
breal* bMulsColor(int bands, const breal* c, breal s, breal* out);
/* Fills every channel of a color with an scalar. */
breal* bFillColor(int bands, breal s, breal* out);

/* Performs a deep copy of an image to `out`. Output is automatically
initiallized if `init_out` is `true`. */
bImage* bCopyImg(const bImage* a, bImage* out, bool init_out);
/* Subtracts two images (pixel-by-pixel) and places result in `out`. */
bImage* bSubImg(const bImage* a, const bImage* b, bImage* out, bool init_out);
/* Adds two images (pixel-by-pixel) and places result in `out`. */
bImage* bAddImg(const bImage* a, const bImage* b, bImage* out, bool init_out);
bImage* bFillImg(bImage* img, const breal* c);

#ifdef __cplusplus
}
#endif

#endif  // MBB_IMAGE_H_
