# MBB (Multi-Band Blending) Plugin for GIMP

## Introduction
This is a plugin for GIMP that implements the multi-band blending technique proposed by Peter Burt and Edward Adelson[^2]. Conceptually speaking, this technique blends images using a different weighting function for each frequency band. It has the benefit of producing more natural transitions between images boundaries than the usual alpha blending technique.

[^2]: Peter J. Burt and Edward H. Adelson. A Multiresolution Spline With Application to Image Mosaics. 1983.

## Features
* Blends two images according to a mask.
* The images and the mask must be in equal-sized and separate layers.
* A new layer is created with the final result.
* By using the selection tool it is possible to restrict blending within some portions of the image.

## Screens/Comparison
### Gimp built-in alpha blending
Bellow is a typical result output by GIMP with layer mode set to "Normal":

<img src="doc/alpla_blending_result.png" width="580"/>

---

### Blending with the MBB plugin
After installation the MBB plugin is available through to the menu `Filters` > `Multi-Band Blending` > `Blend...`:

<img src="doc/menu.png" width="580"/>

The input images and the mask are specified in the settings dialog. Note that the MBB plugin requires the mask is in a separate layer (rather than in a layer mask as the GIMP built-in blending):

<img src="doc/dialog.png" width="480"/>

The MBB plugin outputs the blended image into a new layer:

<img src="doc/mbb_result.png" width="580"/>

## Changelog
### v0.1.1 (2017-05-07)
* Minor bug fixes.

### v0.1 (2014-04-20)
* Initial release.

## [Download](https://gitlab.com/saulopessoa/gimp-mbb/tags)

## Installation
### Windows
To a user based installation, extract the plugin file to your GIMP user plugin directory (usually `%Homedrive%%Homepath%\.gimp-2.8\plug-ins`). For a system-wide installation, extract the plugin to the system GIMP plugin directory (usually `<root>\Program Files\GIMP 2\lib\gimp\2.0\plug-ins`).

### Ubuntu
To a user based installation, extract the plugin file to your GIMP user plugin directory (usually `$HOME/.gimp-2.8/plug-ins`).

## Observations
The plugin was tested with GIMP version 2.8.x.

## Author
Saulo A. Pessoa (<saulopessoa@gmail.com>)
