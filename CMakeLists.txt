cmake_minimum_required(VERSION 2.8)

project(mbb)

set(MBB_DEPENDENCIES_PREFIX CACHE PATH "Path to dependencies. For example, on Linux systems it is usually the path /usr/.")

if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
  set(MBB_LIB_INCLUDE_DIR)
  set(MBB_PLATFORM_SHORT win32)

  # Adds linker flags for `exe` targets. The flag `-mwindows` disables command
  # prompt emergence.
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -mwindows" )
else ()  # Linux
  set(MBB_LIB_INCLUDE_DIR x86_64-linux-gnu)
  set(MBB_PLATFORM_SHORT x11)
endif()

message("Target system selected: ${CMAKE_SYSTEM_NAME}")

################################################################################
# Targets.
set(MBB_LIB ${PROJECT_NAME}-lib)
set(MBB_LIB_TEST ${PROJECT_NAME}-libtest)
set(MBB_GIMP_PLUGIN ${PROJECT_NAME})

# Specifies compiler flags for both C and C++.
add_definitions("-Wall")

################################################################################
# Settings of the MBB library from here.
include_directories(
  "${CMAKE_CURRENT_SOURCE_DIR}/src"
)

add_library(${MBB_LIB}
  "src/mbb/image.c"
  "src/mbb/image.h"
  "src/mbb/image_pyramid.c"
  "src/mbb/image_pyramid.h"
  "src/mbb/math.c"
  "src/mbb/math.h"
  "src/mbb/mbb.c"
  "src/mbb/mbb.h"
  "src/mbb/prerequisites.h"
)
set_target_properties(${MBB_LIB} PROPERTIES OUTPUT_NAME mbb)
set_property(TARGET ${MBB_LIB} PROPERTY C_STANDARD 99)

################################################################################
# Settings of the MBB tests from here.

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
  find_package(GTest REQUIRED)
  include_directories(${GTEST_INCLUDE_DIRS})

  find_package(Boost REQUIRED)
  include_directories(${Boost_INCLUDE_DIRS})

  add_executable(${MBB_LIB_TEST}
    "src/test/main.cpp"
    "src/test/mbb/image.cpp"
    "src/test/mbb/image_pyramid.cpp"
    "src/test/mbb/math.cpp"
    "src/test/mbb/mbb.cpp"
  )

  target_link_libraries(${MBB_LIB_TEST}
    ${MBB_LIB}
    ${GTEST_LIBRARIES}
    ${Boost_LIBRARIES}
    png
    pthread
  )

  # Enables c++11.
  target_compile_features(${MBB_LIB_TEST} PRIVATE cxx_range_for)
endif()

################################################################################
# Settings of the MBB Gimp plugin from here.
include_directories(
  "${MBB_DEPENDENCIES_PREFIX}/include/atk-1.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/cairo"
  "${MBB_DEPENDENCIES_PREFIX}/include/freetype2"
  "${MBB_DEPENDENCIES_PREFIX}/include/gdk-pixbuf-2.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/gimp-2.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/gio-unix-2.0/"
  "${MBB_DEPENDENCIES_PREFIX}/include/glib-2.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/gtk-2.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/libpng12"
  "${MBB_DEPENDENCIES_PREFIX}/include/pango-1.0"
  "${MBB_DEPENDENCIES_PREFIX}/include/pixman-1"
  "${MBB_DEPENDENCIES_PREFIX}/lib/${MBB_LIB_INCLUDE_DIR}/glib-2.0/include"
  "${MBB_DEPENDENCIES_PREFIX}/lib/${MBB_LIB_INCLUDE_DIR}/gtk-2.0/include"
)

link_directories(${MBB_DEPENDENCIES_PREFIX}/lib/)

add_executable(${MBB_GIMP_PLUGIN}
  "src/plugin/config.h"
  "src/plugin/error.c"
  "src/plugin/error.h"
  "src/plugin/main.c"
  "src/plugin/util.h"
)

target_link_libraries(${MBB_GIMP_PLUGIN}
  ${MBB_LIB}
  atk-1.0
  cairo
  fontconfig
  freetype
  gdk_pixbuf-2.0
  gdk-${MBB_PLATFORM_SHORT}-2.0
  gimp-2.0
  gimpbase-2.0
  gimpcolor-2.0
  gimpconfig-2.0
  gimpmath-2.0
  gimpmodule-2.0
  gimpui-2.0
  gimpwidgets-2.0
  gio-2.0
  glib-2.0
  gobject-2.0
  gtk-${MBB_PLATFORM_SHORT}-2.0
  pango-1.0
  pangocairo-1.0
  pangoft2-1.0
)
